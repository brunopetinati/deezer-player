import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";

import { getPlaylistThunk } from "../../store/modules/data-songs/thunk";
import { StyledButton } from "../../styles/index";
import { Container } from "./styles";

import { motion } from "framer-motion";

const Playlists = () => {

const history = useHistory();


const dispatch = useDispatch();

const handleClick = (playlist) => {

    dispatch(getPlaylistThunk(playlist))
    history.push('/')
};

return (
    <motion.div
    initial={{ opacity: 0 }}
    animate={{ opacity: 1 }}
    exit={{ opacity: 0 }}
    transition={{ duration: 3 }}
  >
    <Container>

    <StyledButton onClick={() => {handleClick('7932030762')}}>Top World</StyledButton>
    <StyledButton onClick={() => {handleClick('3155776842')}}>Top WorldWide</StyledButton> 
    <StyledButton onClick={() => {handleClick('4461060364')}}>Global 2021 Hits</StyledButton>                                                                                                                                                                                                                                                                                                                                                  
    </Container>

    </ motion.div>
)


};

export default Playlists;