import ListSongs from "../../components/songs-list/index";
import InputSearch from "../../components/input-search/index";
import { motion } from "framer-motion";


const Home = () => {

    return (<motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
        transition={{ duration: 2 }}
      >    
    <InputSearch title={"Enjoy the Top World Songs by Deezer!"}/>

    <ListSongs />

    </motion.div>)
}

export default Home;